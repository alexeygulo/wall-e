class Description {
  identificationString = 'Client-side issue'

  constructor() {
    this.linkElement = document.querySelector('.js-task-list-container a.gfm.gfm-issue')
    this.parentLinkElement = this.linkElement?.parentElement
    this.containerElement = document.querySelector('.description')

    this._init()
  }

  _init() {
    if (!this._isLinkIdentified()) return

    this.spinner = this._createSpinner()

    const buttonElement = this._createButton()
    this.parentLinkElement.append(' — ', buttonElement)
  }

  _createButton() {
    const linkElement = document.createElement('a')

    linkElement.setAttribute('role', 'button')
    linkElement.textContent = 'Show Description'
    linkElement.append(this.spinner.el)

    linkElement.addEventListener('click', this._handleClickButton.bind(this), { once : true })

    return linkElement
  }

  _isLinkIdentified() {
    return this.linkElement && this.parentLinkElement?.textContent.includes(this.identificationString)
  }

  async _handleClickButton(event) {
    const currentTarget = event.currentTarget

    this.spinner.show()
    currentTarget.classList.add('text-muted')

    await this.renderDescription()

    this.spinner.hide()
  }

  _createDescriptionWrap(content) {
    const wrapElement = document.createElement('div')

    wrapElement.classList.add('mt-5')
    wrapElement.innerHTML = content

    return wrapElement
  }

  _createSpinner() {
    const spinnerElement = document.createElement('span')

    spinnerElement.classList.value = 'gl-spinner gl-spinner-orange gl-spinner-sm gl-vertical-align-text-bottom ml-2 d-none'

    return {
      el: spinnerElement,
      show: () => spinnerElement.classList.remove('d-none'),
      hide: () => spinnerElement.classList.add('d-none')
    }
  }

  async _fetchData() {
    const url = this.linkElement.href + '/realtime_changes'

    const response = await fetch(url)
    const data = await response.json()

    return data
  }

  async renderDescription() {
    const { description } = await this._fetchData()
    const descriptionWrapElement = this._createDescriptionWrap(description)

    this.containerElement.append(descriptionWrapElement)
  }
}

class ChecklistGenerator {
  checklist = ''
  editDescriptionButtonSelector = '.js-issuable-edit'
  saveChangesButtonSelector = '.qa-save-button'
  commentSelector = '.note.note-wrapper:not(.system-note)'
  commentTextSelector = '.note-text'
  commentElements = []

  constructor () {
    this._init()
  }

  _init () {
    this._findComments()

    const editDescriptionButtonElement = document.querySelector(this.editDescriptionButtonSelector)
    if (!editDescriptionButtonElement) return

    editDescriptionButtonElement.addEventListener('click', this.handleClickEditDescriptionButton.bind(this))
  }

  handleClickEditDescriptionButton () {
    setTimeout(() => {
      const saveChangesButtonElement = document.querySelector(this.saveChangesButtonSelector)
      const checklistGeneratorButtonElement = this._createChecklistGeneratorButton()

      saveChangesButtonElement.after(checklistGeneratorButtonElement)
    }, 2000)
  }

  handleClickChecklistGeneratorButton () {
    const commentsElement = this._findComments()

    const checklistItems = commentsElement.map((item) => {
      return this._generateChecklistItem(item)
    })

   this._generateChecklist(checklistItems)
  }

  _createChecklistGeneratorButton () {
    const buttonElement = document.createElement('button')
    buttonElement.type = 'button'
    buttonElement.classList.value = 'btn btn-confirm btn-md float-left gl-button ml-2'
    buttonElement.textContent = 'Checklist generate'

    buttonElement.addEventListener('click', this.handleClickChecklistGeneratorButton.bind(this))

    return buttonElement
  }

  _findComments () {
    return [...document.querySelectorAll(this.commentSelector)]
  }

  _generateChecklistItem (commentElement) {
    const contentCommentElement = commentElement.querySelector(this.commentTextSelector)

    if (!contentCommentElement) return

    const contentComment = contentCommentElement.textContent
    const { id } = commentElement

    const checklistItem = `-[ ] ${contentComment} ${location.href}#${id}`

    return checklistItem
  }

  _generateChecklist (items) {
    items.forEach((item) => {
      this.checklist = this.checklist + item + '\n'
    })
  }
}

// TODO:
// new ChecklistGenerator()

if (location.href.includes('skdo.works')) {
  document.body.classList.add('skdo')
  new Description()
}
